# Microscopy Image Dataset for Deep Learning-Based Quantitative Assessment of Pulmonary Vascular Changes

## Dataset

### About Dataset

The Vessels segmentation dataset was used in the paper "Microscopy Image Dataset for Deep Learning-Based Quantitative Assessment of Pulmonary Vascular Changes".

### Download

Links to downloading dataset:

The dataset is publicity available at [figshare](https://figshare.com/articles/dataset/Pulmonary_Circulation_Vessels_Dataset_for_pathology_ssessment_using_Machine_Learning-based_Image_Segmentation/24968940).

### Dataset folder
It includes 609 original microphotographs of vessels, numerical data from experts' measurements, microphotographs with outlines of these measurements and binary masks for each of the vessels.  

<table>
  <tr>
    <td><img src="images/img_ds_image_1.png" alt="Original microscopic"></td>
    <td><img src="images/img_ds_mask_1.png" alt="Microscopic mask"></td>
  </tr>
</table>

- Dataset
    - Images.zip -- Raw microscopic images.
      - <image_name>.png
      - ...
    - Masks.zip -- Binary segmentation masks.
      - <image_name>.png
      - ...
    - Blended -- Blended raw images with segmentation masks.
      - <image_name>.png
      - ...
    - Image with outline -- Images with experts outline.
      - <image_name>.png
      - ...
    - Base.xlsx

### Usage Notes

To see an example of using a segmentation-related part of the dataset, refer to the [pipeline](https://gitlab.com/digiratory/biomedimaging/pcv-dataset/-/blob/main/train_example.ipynb?ref_type=heads) file with an example of neural network training and usage.

# Licence

The source code is covered by MIT Licence.

The dataset is covered by [Creative Commons CC0](https://creativecommons.org/share-your-work/public-domain/cc0) Licence.


# Citation

If you find this project useful, please cite:

```bib

```
